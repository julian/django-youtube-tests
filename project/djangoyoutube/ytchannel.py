#!/usr/bin/python3

# Simple XML parser for YouTube XML channels
# Jesus M. Gonzalez-Barahona <jgb @ gsyc.es> 2020
# SARO and SAT subjects (Universidad Rey Juan Carlos)
#
# Example of XML document for a YouTube channel:
# https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

class YTHandler(ContentHandler):

    def __init__(self):
        self.inEntry = False  # Para saber si estamos dentro de entry
        self.inContent = False  # Si tenemos contenido que queremos leer
        self.inMediaGroup = False
        self.inAuthor = False
        self.content = ""
        self.title = ""
        self.link = ""
        self.videoId = ""
        self.fotoVideo = ""
        self.nombreCanal = ""
        self.enlaceCanal = ""
        self.fecha = ""
        self.descripcion = ""
        self.videos = []

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'media:group':
                self.inMediaGroup = True
            elif name == 'author':
                self.inAuthor = True
            elif name == 'published':
                self.inContent = True
        if self.inMediaGroup:
            if name == 'media:thumbnail':
                self.fotoVideo = attrs.get('url')
            if name == 'media:description':
                self.inContent = True
        if self.inAuthor:
            if name == 'name':
                self.inContent = True
            elif name == 'uri':
                self.inContent = True

    def endElement(self, name):
        if name == 'entry':
            from .models import VideoSeleccionables
            self.inEntry = False
            newVideo = VideoSeleccionables(nombre=self.title, enlace=self.link, videoId=self.videoId,
                                           fotoVideo=self.fotoVideo, nombreCanal=self.nombreCanal,
                                           enlaceCanal=self.enlaceCanal, fecha=self.fecha,
                                           descripcion=self.descripcion)
            newVideo.save()
            self.videos.append({'link': self.link,
                                'title': self.title,
                                'id': self.videoId})

        elif name == 'media:group':
            self.inMediaGroup = False

        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.videoId = self.content
                self.content = ""
                self.inContent = False
            elif name == 'name':
                self.nombreCanal = self.content
                self.content = ""
                self.inContent = False
            elif name == 'uri':
                self.enlaceCanal = self.content
                self.content = ""
                self.inContent = False
            elif name == 'published':
                self.fecha = self.content
                self.content = ""
                self.inContent = False
            elif name == 'media:description':
                self.descripcion = self.content
                self.content = ""
                self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars

class YTChannel:
    """Class to get videos in a YouTube channel.

    Extracts video links and titles from the XML document for a YT channel.
    The list of videos found can be retrieved lated by calling videos()
    """

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos (self):

        return self.handler.videos
