
from django.apps import AppConfig

import urllib.request
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from .ytchannel import YTChannel

# Create your views here.


def CleanDB():
    from .models import VideoSeleccionados, VideoSeleccionables
    VideoSeleccionados.objects.all().delete()
    VideoSeleccionables.objects.all().delete()


class DjangoyoutubeConfig(AppConfig):
    name = 'djangoyoutube'

    def ready(self):
        CleanDB()
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
              + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        channel = YTChannel(xmlStream)
        videos = channel.videos()



