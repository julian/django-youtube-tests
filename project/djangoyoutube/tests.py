import urllib.request

from django.test import TestCase, SimpleTestCase

from . import views
from .ytchannel import YTChannel
# TEST PARSE
class TestYTChannel(TestCase):
    def setUp(self):
        self.expected = [{'link': 'https://www.youtube.com/watch?v=SPLwYv62-og',
                     'title': 'JSON práctico',
                     'id': 'SPLwYv62-og'},
                    {'link': 'https://www.youtube.com/watch?v=MyaRcqzBbk4',
                     'title': 'Django práctico: testing',
                     'id': 'MyaRcqzBbk4'}]

    def test_simple(self):
        """Find videos in a simple XML file for a YT channel"""
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
              + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        channel = YTChannel(xmlStream)
        videos = channel.videos()
        self.assertEqual(videos[0:2], self.expected)


#TEST FUNCION AUXILIAR VIEWS
class TestFunc(TestCase):
    def setUp(self):
        self.title = 'JSON práctico'
    """    
    def test_func(self):
        self.assertEqual(True, views.Check(self.title))
    """
    def test_view(self):
        response = self.client.get('/djangoyoutube/')
        self.assertEqual(response.resolver_match.func, views.index)

#RECURSOS DE LA APLICACION
class TestRequest(TestCase):

    def setUp(self):
        self.idErr = 'uBuldIyTo8I'
        self.idOk = 'SPLwYv62-og'

    def test_get_ok(self):
        response = self.client.get('/djangoyoutube/')
        self.assertEqual(response.status_code, 200)

    def test_get_index(self):
        checks = ["<h1>Django-Youtube</h1>",
                  "<h2>Servicio y Apliaciones Telematicas</h2>"]
        response = self.client.get('/djangoyoutube/')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)
"""
    def test_post_content(self):
        checks = ['<h3><a class= "urlvideo" href="https://www.youtube.com/watch?v=MyaRcqzBbk4">Django práctico: testing</a></h3>']
        response = self.client.post('/djangoyoutube/', {'action': 'MyaRcqzBbk4'})
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)
"""